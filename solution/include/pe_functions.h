/**
    @file pe_functions.h
    @brief Functions for file input and output operations
*/


#pragma once

#include <pe_class.h>
/**
    @brief основная программа для поиска секции
    @param input где хранится PE файл
    @param output куда выводить ответ
    @param section_name искомое имя секции
*/
int get_section (FILE* input, FILE* output, const char* section_name);

/**
    @brief считать характеристики Pe файла в структру, которая за него отвечает
    @param input где хранится PE файл
    @param PE_file структура
*/
int read_file (FILE* input, struct PEFile* PE_file);

/**
    @brief вывести HE header
    @param input где хранится PE файл
    @param output куда выводить ответ
    @param sectionHeader характеристики секции, которую нужно вывести
*/
int write_section(FILE* input, FILE* output, const struct SectionHeader* sectionHeader);
