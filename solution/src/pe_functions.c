#include "pe_functions.h"
#include <stdlib.h>
#include <string.h>

int read_file(FILE* const in, struct PEFile* PE_file){
    if (fseek(in, MAIN_DELTA, SEEK_SET) != 0){
        return 0;
    }
    if (!fread(&PE_file->header_offset, sizeof (PE_file->header_offset), 1, in)){
        return 0;
    }
    if (fseek(in, PE_file->header_offset, SEEK_SET) != 0){
        return 0;
    }
    if (!fread(&PE_file->header, sizeof(struct PEHeader), 1, in)) {
        return 0;
    }
    if (PE_file->header.magic != PE_MAGIC_NUMBER){
        return 0;
    }
    if (fseek(in, PE_file->header.sizeOfOptionalHeader, SEEK_CUR) != 0){
        return 0;
    }

    PE_file->section_headers = malloc(PE_file->header.numberOfSections * sizeof(struct SectionHeader));


    for (uint16_t i = 0; i < PE_file->header.numberOfSections; i++) {
        if (!fread(&PE_file->section_headers[i], sizeof(struct SectionHeader), 1, in)) {
            return 0;
        }
    }
    return 1;
}

int get_section (FILE* input, FILE* output, const char* section_name){
    struct PEFile peFile;
    int reading_flag = read_file(input, &peFile);

    if(reading_flag){
        for(int i = 0;i < peFile.header.numberOfSections;i++){
            if(!strcmp(section_name, peFile.section_headers[i].name)){
                int write_flag = write_section(input, output, &peFile.section_headers[i]);
                free(peFile.section_headers);
                if(write_flag)
                    return 0;
                else
                    return 1;
            }

        }
        free(peFile.section_headers);
    }
    return 1;

}

int write_section(FILE* input, FILE* output, const struct SectionHeader* sectionHeader){
    if(fseek(input, sectionHeader->pointerToRawData, SEEK_SET)){
        return 0;
    }

    char* write_buffer[sectionHeader->sizeOfRawData];

    if(!fread(write_buffer, sectionHeader->sizeOfRawData, 1, input)){
        return 0;
    }

    if(!fwrite(write_buffer, sectionHeader->sizeOfRawData, 1, output)){
        return 0;
    }
    
    return 1;
}

